<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;


/**
 * Class CalculateWallet
 * @package App\Jobs
 */
class CalculateWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected array $payment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payment)
    {
        $this->payment = $payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $result = [
                "id" => $this->payment["id"],
                "user_id" => (int)$this->payment["order_number"],
                "sum" => round($this->payment["sum"] - ($this->payment["sum"] * ($this->payment["commision"] / 100))),
            ];
            DB::table('user_wallet')->insert($result);
            echo $result['id'];
        } catch (\Exception $e) {
            dump($e);
        }
    }
}
