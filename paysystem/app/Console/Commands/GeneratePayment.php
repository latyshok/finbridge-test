<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Ramsey\Uuid\Uuid;

class GeneratePayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $items = [];
        $hash = '';
        $secret_key = env('PAYMENT_SECRET_KEY');

        for ($i = 1; $i <= rand(1, 10); $i++) {
            $uuid = Uuid::uuid4();
            $item = [
                "id" => $uuid,
                "sum" => $this->genSum(),
                "commision" => $this->genCommision(),
                "order_number" => $this->genOrderNumber(),
            ];
            $hash .= $uuid->toString();
            $items[] = $item;
        }

        DB::table('transactions')->insert($items);

        Http::post(env('API_URL'), $items);

        echo $response->json();
    }

    private function genSum(): int
    {
        return rand(10, 5000);
    }

    private function genCommision(): float
    {
        return rand(5, 20) / 10;
    }

    private function genOrderNumber(): int
    {
        return rand(1, 20);
    }
}
