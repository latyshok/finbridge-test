# Finbridge test

Реализация тестового задания от finbridge

Выполнить след. команды:

Создать .env из .env_example

docker run --rm -v %cd%:/app composer install \
docker-compose up -d \
docker-compose exec paysystem php artisan migrate \
docker-compose exec payapi php artisan migrate \
docker-compose exec payapi php artisan queue:work 

Крон на сервере:
```
* * * * * docker-compose exec paysystem php artisan generate:payments
* * * * * ( sleep 20 ; docker-compose exec paysystem php artisan generate:payments )
* * * * * ( sleep 40 ; docker-compose exec paysystem php artisan generate:payments )
```
